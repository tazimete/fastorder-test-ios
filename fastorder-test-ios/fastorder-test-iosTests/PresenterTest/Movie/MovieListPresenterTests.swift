//
//  PresenterTests.swift
//  fastorder-test-iosTests
//
//  Created by Mostafizur Rahman on 1/5/20.
//  Copyright © 2020 FastOrder. All rights reserved.
//

import XCTest
import Foundation
@testable import fastorder_test_ios

class MovieListPresenterTests: XCTestCase {
    
    var presenter: MovieListPresenter?
    var mockInteractor: MockMovieListInteractor?
    var mockRouter: MockMovieListRouter?
    var movieList: [Movie]!
    var mockView: MockMovieListView?
    
    override func setUp() {
        super.setUp()
        
        movieList = [Movie]()
        let movie1 = Movie()
        movie1.id = Optional(100)
        movie1.posterPath = Optional("\(ApiManager.IMAGE_SERVER_BASE_URL)/uCDcQbfIKY4oTPd6tbghloFm7Gi.jpg)")
        movie1.overview = Optional("Set in the dazzling world of the LA music scene comes the story of Grace Davis, a superstar whose talent, and ego, have reached unbelievable heights. Maggie is Grace overworked personal assistant who stuck running errands, but still aspires to her childhood dream of becoming a music producer. When Grace manager presents her with a choice that could alter the course of her career, Maggie and Grace come up with a plan that could change their lives forever.")
        movie1.originalLanguage = Optional("en")
        movie1.title = Optional("The High Note")
        movie1.popularity = Optional("22.401")
        movie1.voteCount = Optional(100)
        movie1.releaseDate = Optional("2020-05-08")
        movie1.category = Optional(MovieCategoryTitle.POPULAR.rawValue)
        movieList.append(movie1)
        
        let movie2 = Movie()
        movie2.id = Optional(200)
        movie2.posterPath = Optional("\(ApiManager.IMAGE_SERVER_BASE_URL)/cQbfIKY4oTPd6tbghloFm7Gi.jpg)")
        movie2.overview = Optional("Set in the dazzling world of the LA music scene comes the story of Grace Davis")
        movie2.originalLanguage = Optional("en")
        movie2.title = Optional("Extraction")
        movie2.popularity = Optional("52.401")
        movie2.voteCount = Optional(500)
        movie2.releaseDate = Optional("2020-04-20")
        movie2.category = Optional(MovieCategoryTitle.TOP_RATED.rawValue)
        movieList.append(movie2)
        
        let movie3 = Movie()
        movie3.id = Optional(200)
        movie3.posterPath = Optional("\(ApiManager.IMAGE_SERVER_BASE_URL)/mofcQbfIKY4oTPd6tbghloFm7Gi.jpg)")
        movie3.overview = Optional("When Grace manager presents her with a choice that could alter the course of her career, Maggie and Grace come up with a plan that could change their lives forever.")
        movie3.originalLanguage = Optional("en")
        movie3.title = Optional("Man Of Steel")
        movie3.popularity = Optional("92.401")
        movie3.voteCount = Optional(1500)
        movie3.releaseDate = Optional("2014-04-20")
        movie3.category = Optional(MovieCategoryTitle.UPCOMMING.rawValue)
        movieList.append(movie3)
        
        //init component
        mockInteractor = MockMovieListInteractor()
        mockRouter = MockMovieListRouter()
        presenter = MovieListPresenter()
        mockView = MockMovieListView()
        presenter?.view = mockView
        presenter?.router = mockRouter
        presenter?.interactor = mockInteractor
        
        //loading movie list
        loadMovieData()
       
    }
    
    override func tearDown() {
        super.tearDown()
        
        mockInteractor = nil
        mockRouter = nil
        presenter = nil
        mockView = nil
        presenter?.view = nil
        presenter?.router = nil
        presenter?.interactor = nil
        movieList = nil
    }
    
    func loadMovieData() -> Void{
        presenter?.didPopularMovieDataLoaded(movieList: movieList.filter({($0.category?.lowercased().contains(MovieCategoryTitle.POPULAR.rawValue.lowercased()) ?? false)}))
        presenter?.didTopRatedMovieDataLoaded(movieList: movieList.filter({($0.category?.lowercased().contains(MovieCategoryTitle.TOP_RATED.rawValue.lowercased()) ?? false)}))
        presenter?.didUpcommingMovieDataLoaded(movieList: movieList.filter({($0.category?.lowercased().contains(MovieCategoryTitle.UPCOMMING.rawValue.lowercased()) ?? false)}))
    }
    
    func testMovieCount() -> Void {
        XCTAssertEqual(presenter?.popularMovieList?.count ?? 0, 1)
        XCTAssertEqual(presenter?.topRatedMovieList?.count ?? 0, 1)
        XCTAssertEqual(presenter?.upcommingMovieList?.count ?? 0, 1)
    }
    
    func testMovieCategory() -> Void {
        XCTAssertEqual(presenter?.getPopularMovie(at: 0)?.category ?? "", MovieCategoryTitle.POPULAR.rawValue)
        XCTAssertEqual(presenter?.getTopRatedMovie(at: 0)?.category ?? "", MovieCategoryTitle.TOP_RATED.rawValue)
        XCTAssertEqual(presenter?.getUpcommingMovie(at: 0)?.category ?? "", MovieCategoryTitle.UPCOMMING.rawValue)
    }
    
    func testMovieTitle() -> Void {
        XCTAssertEqual(presenter?.getPopularMovie(at: 0)?.title ?? "", "The High Note")
        XCTAssertEqual(presenter?.getTopRatedMovie(at: 0)?.title ?? "", "Extraction")
        XCTAssertEqual(presenter?.getUpcommingMovie(at: 0)?.title ?? "", "Man Of Steel")
    }
    
    func testSelectToShowMovieDetails() -> Void {
        presenter?.showMovieDetailsViewController(with: presenter?.popularMovieList?[0] ?? Movie(), from: UIViewController())
        XCTAssertEqual(presenter?.router?.movie?.title ?? "", "The High Note")
        XCTAssertEqual(presenter?.router?.movie?.category ?? "", MovieCategoryTitle.POPULAR.rawValue)
        XCTAssertEqual(presenter?.router?.movie?.voteCount ?? 0, 100)
        XCTAssertEqual(presenter?.router?.movie?.releaseDate ?? "", "2020-05-08")
        
        presenter?.showMovieDetailsViewController(with: presenter?.topRatedMovieList?[0] ?? Movie(), from: UIViewController())
        XCTAssertEqual(presenter?.router?.movie?.title ?? "", "Extraction")
        XCTAssertEqual(presenter?.router?.movie?.category ?? "", MovieCategoryTitle.TOP_RATED.rawValue)
        XCTAssertEqual(presenter?.router?.movie?.voteCount ?? 0, 500)
        XCTAssertEqual(presenter?.router?.movie?.releaseDate ?? "", "2020-04-20")
        
        presenter?.showMovieDetailsViewController(with: presenter?.upcommingMovieList?[0] ?? Movie(), from: UIViewController())
        XCTAssertEqual(presenter?.router?.movie?.title ?? "", "Man Of Steel")
        XCTAssertEqual(presenter?.router?.movie?.category ?? "", MovieCategoryTitle.UPCOMMING.rawValue)
        XCTAssertEqual(presenter?.router?.movie?.voteCount ?? 0, 1500)
        XCTAssertEqual(presenter?.router?.movie?.releaseDate ?? "", "2014-04-20")
    }
    
    func testSearchPopularMovie() -> Void {
        //search popular movie
        let textPopular = "The High Note"
        presenter?.searchMovieByTitle(searchText: textPopular)
        XCTAssertEqual(presenter?.getPopularMovie(at: 0)?.title ?? "", "The High Note")
        XCTAssertEqual(presenter?.getPopularMovie(at: 0)?.category ?? "", MovieCategoryTitle.POPULAR.rawValue)
        XCTAssertEqual(presenter?.getPopularMovie(at: 0)?.voteCount ?? 0, 100)
        XCTAssertEqual(presenter?.getPopularMovie(at: 0)?.releaseDate ?? "", "2020-05-08")
        XCTAssertNotEqual(presenter?.getPopularMovie(at: 0)?.releaseDate ?? "", "2020-04-20")
        
        let textTopRated = "Extraction"
        presenter?.searchMovieByTitle(searchText: textTopRated)
        XCTAssertEqual(presenter?.getTopRatedMovie(at: 0)?.title ?? "", "Extraction")
        XCTAssertEqual(presenter?.getTopRatedMovie(at: 0)?.category ?? "", MovieCategoryTitle.TOP_RATED.rawValue)
        XCTAssertEqual(presenter?.getTopRatedMovie(at: 0)?.voteCount ?? 0, 500)
        XCTAssertEqual(presenter?.getTopRatedMovie(at: 0)?.releaseDate ?? "", "2020-04-20")
        XCTAssertNotEqual(presenter?.getTopRatedMovie(at: 0)?.releaseDate ?? "", "2014-04-20")
        
        let textUpcomming = "Man Of Steel"
        presenter?.searchMovieByTitle(searchText: textUpcomming)
        XCTAssertEqual(presenter?.getUpcommingMovie(at: 0)?.title ?? "", "Man Of Steel")
        XCTAssertEqual(presenter?.getUpcommingMovie(at: 0)?.category ?? "", MovieCategoryTitle.UPCOMMING.rawValue)
        XCTAssertEqual(presenter?.getUpcommingMovie(at: 0)?.voteCount ?? 0, 1500)
        XCTAssertEqual(presenter?.getUpcommingMovie(at: 0)?.releaseDate ?? "", "2014-04-20")
        XCTAssertNotEqual(presenter?.getUpcommingMovie(at: 0)?.releaseDate ?? "", "2020-05-08")
    }
    
    func testResetMovieList() -> Void {
        presenter?.resetMovieData()
        XCTAssertEqual(presenter?.popularMovieList?.count ?? 0, 1)
        XCTAssertEqual(presenter?.topRatedMovieList?.count ?? 0, 1)
        XCTAssertEqual(presenter?.upcommingMovieList?.count ?? 0, 1)
    }
}

class MockMovieListPresenter : MovieListPresenter{
    override func didPopularMovieDataLoaded(movieList: [Movie]) {
        popularMovieList = movieList
    }
    
    override func didTopRatedMovieDataLoaded(movieList: [Movie]) {
        topRatedMovieList = movieList
    }
    
    override func didUpcommingMovieDataLoaded(movieList: [Movie]) {
        upcommingMovieList = movieList
    }
}


class MockMovieListView: MovieListViewController {
    var popularMovieList = [Movie](), topRatedMovieList = [Movie](), upcommingMovieList = [Movie]()
    
    override func initialize() {
        
    }
    
    override func didPopularMovieDataLoaded(movieList: [Movie]) {
        popularMovieList = movieList
    }
    
    override func didTopRatedMovieDataLoaded(movieList: [Movie]) {
        topRatedMovieList = movieList
    }
    
    override func didUpcommingMovieDataLoaded(movieList: [Movie]) {
        upcommingMovieList = movieList
    }
    
    override func didSearchComplete() {
        
    }
    
    override func didResetMovieData() {
        
    }
    
}


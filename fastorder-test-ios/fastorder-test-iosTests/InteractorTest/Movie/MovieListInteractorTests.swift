//
//  Interactor.swift
//  fastorder-test-iosTests
//
//  Created by Mostafizur Rahman on 1/5/20.
//  Copyright © 2020 FastOrder. All rights reserved.
//

import XCTest
import Foundation
@testable import fastorder_test_ios

class MovieListInteractorTests: XCTestCase {
    var presenter: MockMovieListPresenter?
    var mockInteractor: MockMovieListInteractor?
    var movieList: [Movie]!
    
    override func setUp() {
        super.setUp()
        
        movieList = [Movie]()
        let movie1 = Movie()
        movie1.id = Optional(100)
        movie1.posterPath = Optional("\(ApiManager.IMAGE_SERVER_BASE_URL)/uCDcQbfIKY4oTPd6tbghloFm7Gi.jpg)")
        movie1.overview = Optional("Set in the dazzling world of the LA music scene comes the story of Grace Davis, a superstar whose talent, and ego, have reached unbelievable heights. Maggie is Grace overworked personal assistant who stuck running errands, but still aspires to her childhood dream of becoming a music producer. When Grace manager presents her with a choice that could alter the course of her career, Maggie and Grace come up with a plan that could change their lives forever.")
        movie1.originalLanguage = Optional("en")
        movie1.title = Optional("The High Note")
        movie1.popularity = Optional("22.401")
        movie1.voteCount = Optional(100)
        movie1.releaseDate = Optional("2020-05-08")
        movie1.category = Optional(MovieCategoryTitle.POPULAR.rawValue)
        movieList.append(movie1)
        
        let movie2 = Movie()
        movie2.id = Optional(200)
        movie2.posterPath = Optional("\(ApiManager.IMAGE_SERVER_BASE_URL)/cQbfIKY4oTPd6tbghloFm7Gi.jpg)")
        movie2.overview = Optional("Set in the dazzling world of the LA music scene comes the story of Grace Davis")
        movie2.originalLanguage = Optional("en")
        movie2.title = Optional("Extraction")
        movie2.popularity = Optional("52.401")
        movie2.voteCount = Optional(500)
        movie2.releaseDate = Optional("2020-04-20")
        movie2.category = Optional(MovieCategoryTitle.TOP_RATED.rawValue)
        movieList.append(movie2)
        
        let movie3 = Movie()
        movie3.id = Optional(200)
        movie3.posterPath = Optional("\(ApiManager.IMAGE_SERVER_BASE_URL)/mofcQbfIKY4oTPd6tbghloFm7Gi.jpg)")
        movie3.overview = Optional("When Grace manager presents her with a choice that could alter the course of her career, Maggie and Grace come up with a plan that could change their lives forever.")
        movie3.originalLanguage = Optional("en")
        movie3.title = Optional("Man Of Steel")
        movie3.popularity = Optional("92.401")
        movie3.voteCount = Optional(1500)
        movie3.releaseDate = Optional("2014-04-20")
        movie3.category = Optional(MovieCategoryTitle.UPCOMMING.rawValue)
        movieList.append(movie3)
        
        //init component
        mockInteractor = MockMovieListInteractor()
        presenter = MockMovieListPresenter()
        mockInteractor?.presenter = presenter
    }
    
    override func tearDown() {
        super.tearDown()
        
        mockInteractor = nil
        presenter = nil
        mockInteractor?.presenter = nil
        movieList = nil
    }
    
    func loadMovieData() -> Void{
        mockInteractor?.popularMovieList = movieList.filter({($0.category?.lowercased().contains(MovieCategoryTitle.POPULAR.rawValue.lowercased()) ?? false)})
        mockInteractor?.topRatedMovieList = movieList.filter({($0.category?.lowercased().contains(MovieCategoryTitle.TOP_RATED.rawValue.lowercased()) ?? false)})
        mockInteractor?.upcommingMovieList = movieList.filter({($0.category?.lowercased().contains(MovieCategoryTitle.UPCOMMING.rawValue.lowercased()) ?? false)})
    }
    
    func testLoadPopularMovie() -> Void {
        loadMovieData()
        mockInteractor?.loadPopularMovieData(page: 1)
        
        XCTAssertEqual(mockInteractor?.presenter?.getPopularMovie(at: 0)?.title ?? "", "The High Note")
        XCTAssertEqual(mockInteractor?.presenter?.getPopularMovie(at: 0)?.category ?? "", MovieCategoryTitle.POPULAR.rawValue)
        XCTAssertEqual(mockInteractor?.presenter?.getPopularMovie(at: 0)?.voteCount ?? 0, 100)
        XCTAssertEqual(mockInteractor?.presenter?.getPopularMovie(at: 0)?.releaseDate ?? "", "2020-05-08")
        XCTAssertNotEqual(mockInteractor?.presenter?.getPopularMovie(at: 0)?.releaseDate ?? "", "2020-04-20")
    }
    
    func testLoadTopRatedMovie() -> Void {
        loadMovieData()
       mockInteractor?.loadTopRatedMovieData(page: 1)
        
        XCTAssertEqual(mockInteractor?.presenter?.getTopRatedMovie(at: 0)?.title ?? "", "Extraction")
        XCTAssertEqual(mockInteractor?.presenter?.getTopRatedMovie(at: 0)?.category ?? "", MovieCategoryTitle.TOP_RATED.rawValue)
        XCTAssertEqual(mockInteractor?.presenter?.getTopRatedMovie(at: 0)?.voteCount ?? 0, 500)
        XCTAssertEqual(mockInteractor?.presenter?.getTopRatedMovie(at: 0)?.releaseDate ?? "", "2020-04-20")
        XCTAssertNotEqual(mockInteractor?.presenter?.getTopRatedMovie(at: 0)?.releaseDate ?? "", "2014-04-20")
   }
    
    func testLoadUpcommingMovie() -> Void {
        loadMovieData()
       mockInteractor?.loadUpcommingMovieData(page: 1)
        
        XCTAssertEqual(mockInteractor?.presenter?.getUpcommingMovie(at: 0)?.title ?? "", "Man Of Steel")
        XCTAssertEqual(mockInteractor?.presenter?.getUpcommingMovie(at: 0)?.category ?? "", MovieCategoryTitle.UPCOMMING.rawValue)
        XCTAssertEqual(mockInteractor?.presenter?.getUpcommingMovie(at: 0)?.voteCount ?? 0, 1500)
        XCTAssertEqual(mockInteractor?.presenter?.getUpcommingMovie(at: 0)?.releaseDate ?? "", "2014-04-20")
        XCTAssertNotEqual(mockInteractor?.presenter?.getUpcommingMovie(at: 0)?.releaseDate ?? "", "2020-05-08")
   }

}

class MockMovieListInteractor: MovieListInteractor {
    override func loadAllMovieData() {
        presenter?.loadAllMovieData()
    }
    
    override func loadPopularMovieData(page: Int) {
        presenter?.didPopularMovieDataLoaded(movieList: popularMovieList ?? [Movie]())
    }
    
    override func loadTopRatedMovieData(page: Int) {
        presenter?.didTopRatedMovieDataLoaded(movieList: topRatedMovieList ?? [Movie]())
    }
    
    override func loadUpcommingMovieData(page: Int) {
        presenter?.didUpcommingMovieDataLoaded(movieList: upcommingMovieList ?? [Movie]())
    }
}

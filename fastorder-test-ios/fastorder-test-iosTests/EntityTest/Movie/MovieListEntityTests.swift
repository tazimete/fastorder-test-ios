//
//  MovieEntityTest.swift
//  fastorder-test-iosTests
//
//  Created by Mostafizur Rahman on 30/4/20.
//  Copyright © 2020 FastOrder. All rights reserved.
//

import XCTest
import Foundation
@testable import fastorder_test_ios

class MovieListEntityTests: XCTestCase {

    private var movie:Movie!
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        movie = Movie()
        movie.id = Optional(100)
        movie.posterPath = Optional("\(ApiManager.IMAGE_SERVER_BASE_URL)/uCDcQbfIKY4oTPd6tbghloFm7Gi.jpg)")
        movie.adult = Optional(false)
        movie.overview = Optional("Set in the dazzling world of the LA music scene comes the story of Grace Davis, a superstar whose talent, and ego, have reached unbelievable heights. Maggie is Grace overworked personal assistant who stuck running errands, but still aspires to her childhood dream of becoming a music producer. When Grace manager presents her with a choice that could alter the course of her career, Maggie and Grace come up with a plan that could change their lives forever.")
        movie.originalTitle = Optional("The High Note")
        movie.originalLanguage = Optional("en")
        movie.title = Optional("The High Note")
        movie.backdropPath = Optional("/wh3yiCdg1gdUwWhAV7PfF2ROlMr.jpg")
        movie.popularity = Optional("22.401")
        movie.voteCount = Optional(100)
        movie.voteAverage = Optional(50)
        movie.video = Optional(false)
        movie.releaseDate = Optional("2020-05-08")
        movie.category = Optional(MovieCategoryTitle.UPCOMMING.rawValue)
    }

    override func tearDown() {
        super.tearDown()
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        movie = nil
    }

    func testMovie() {
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        XCTAssertEqual(movie.id ?? 0, 100)
        XCTAssertNotEqual(movie.id ?? 0, 0)
        XCTAssertEqual(movie.title ?? "", "The High Note")
        XCTAssertNotEqual(movie.title ?? "", "test")
        XCTAssertEqual(movie.originalLanguage ?? "", "en")
        XCTAssertEqual(movie.popularity ?? "", "22.401")
        XCTAssertEqual(movie.voteCount ?? 0, 100)
        XCTAssertNotEqual(movie.voteCount ?? 0, 0)
        XCTAssertEqual(movie.video ?? false, false)
        XCTAssertEqual(movie.releaseDate ?? "", "2020-05-08")
        XCTAssertEqual(movie.category ?? "", MovieCategoryTitle.UPCOMMING.rawValue)
    }
    
}

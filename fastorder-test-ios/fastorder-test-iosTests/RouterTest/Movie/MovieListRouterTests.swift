//
//  RouterTests.swift
//  fastorder-test-iosTests
//
//  Created by Mostafizur Rahman on 1/5/20.
//  Copyright © 2020 FastOrder. All rights reserved.
//

import XCTest
import Foundation
@testable import fastorder_test_ios

class MovieListRouterTests: XCTestCase {
    var mockMovielistRouter: MockMovieListRouter?
    var movie: Movie?
    
    override func setUp() {
        super.setUp()
        
        movie = Movie()
        movie?.id = Optional(200)
        movie?.posterPath = Optional("\(ApiManager.IMAGE_SERVER_BASE_URL)/mofcQbfIKY4oTPd6tbghloFm7Gi.jpg)")
        movie?.overview = Optional("When Grace manager presents her with a choice that could alter the course of her career, Maggie and Grace come up with a plan that could change their lives forever.")
        movie?.originalLanguage = Optional("en")
        movie?.title = Optional("Man Of Steel")
        movie?.popularity = Optional("92.401")
        movie?.voteCount = Optional(1500)
        movie?.releaseDate = Optional("2014-04-20")
        movie?.category = Optional(MovieCategoryTitle.UPCOMMING.rawValue)
        
        mockMovielistRouter = MockMovieListRouter()
    }
    
    override func tearDown() {
        super.tearDown()
        
        mockMovielistRouter = nil
        movie = nil
    }
    
    func testShowMovieDetails() -> Void {
        mockMovielistRouter?.showMovieDetailsViewController(with: movie ?? Movie(), from: UIViewController())
        XCTAssertEqual(mockMovielistRouter?.movie?.title ?? "", "Man Of Steel")
        XCTAssertEqual(mockMovielistRouter?.movie?.category ?? "", MovieCategoryTitle.UPCOMMING.rawValue)
        XCTAssertEqual(mockMovielistRouter?.movie?.voteCount ?? 0, 1500)
        XCTAssertEqual(mockMovielistRouter?.movie?.releaseDate ?? "", "2014-04-20")
        
        XCTAssertNotEqual(mockMovielistRouter?.movie?.title ?? "", "EXTRACTION")
        XCTAssertNotEqual(mockMovielistRouter?.movie?.category ?? "", MovieCategoryTitle.POPULAR.rawValue)
        XCTAssertNotEqual(mockMovielistRouter?.movie?.voteCount ?? 0, 100)
        XCTAssertNotEqual(mockMovielistRouter?.movie?.releaseDate ?? "", "2020-04-20")
    }

}

class MockMovieListRouter: MovieListRouter {
    
    override func showMovieDetailsViewController(with movie: Movie, from view: UIViewController) {
        self.movie = Optional(movie)
    }
}

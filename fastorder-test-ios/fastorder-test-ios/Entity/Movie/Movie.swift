//
//  Movie.swift
//  fastorder-test-ios
//
//  Created by Mostafizur Rahman on 28/4/20.
//  Copyright © 2020 FastOrder. All rights reserved.
//

import UIKit

public class Movie: NSObject {
    open var id:Int?
    open var posterPath:String?
    open var adult:Bool?
    open var overview:String?
    open var originalTitle:String?
    open var originalLanguage:String?
    open var title:String?
    open var backdropPath:String?
    open var popularity:String?
    open var releaseDate:String?
    open var voteCount:Int?
    open var video:Bool?
    open var voteAverage:Int?
    open var category:String?
}

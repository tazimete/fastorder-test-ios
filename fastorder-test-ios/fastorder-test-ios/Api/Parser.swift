//
//  Parser.swift
//  fastorder-test-ios
//
//  Created by Mostafizur Rahman on 29/4/20.
//  Copyright © 2020 FastOrder. All rights reserved.
//

import UIKit
import CoreData

class Parser {
    //pasre movie api response
    public static func parseMovieApiResponse(response:NSDictionary?, category:String?) -> [Movie]{
        var movieList = [Movie]()
        let movieJsonList = response?["results"] as? [NSDictionary]
        
        for movieJson in movieJsonList ?? [NSDictionary](){
            let movie = Movie()
            movie.id = movieJson["id"] as? Int
            movie.posterPath = "\(ApiManager.IMAGE_SERVER_BASE_URL)\((movieJson["poster_path"] as? String) ?? "")"
            movie.adult = movieJson["adult"] as? Bool
            movie.overview = movieJson["overview"] as? String
            movie.originalTitle = movieJson["original_title"] as? String
            movie.originalLanguage = movieJson["original_language"] as? String
            movie.title = movieJson["title"] as? String
            movie.backdropPath = movieJson["backdrop_path"] as? String
            movie.popularity = movieJson["popularity"] as? String
            movie.voteCount = movieJson["vote_count"] as? Int
            movie.voteAverage = movieJson["vote_average"] as? Int
            movie.video = movieJson["video"] as? Bool
            movie.releaseDate = movieJson["release_date"] as? String
            movie.category = category

            movieList.append(movie)
        }
        
        return movieList
    }
    
    //pasre movie data manage object list to movie
    public static func parseMovieDataListToMovie(movieDataList:[NSManagedObject]) -> [Movie]{
        var movieList = [Movie]()
        
        for movieData in movieDataList{
            let movie = Movie()
            movie.id = movieData.value(forKey: "id") as? Int
            movie.posterPath = movieData.value(forKey: "posterPath") as? String
            movie.adult = movieData.value(forKey: "adult") as? Bool
            movie.overview = movieData.value(forKey: "overview") as? String
            movie.originalTitle = movieData.value(forKey: "originalTitle") as? String
            movie.originalLanguage = movieData.value(forKey: "originalLanguage") as? String
            movie.title = movieData.value(forKey: "title") as? String
            movie.backdropPath = movieData.value(forKey: "backdropPath") as? String
            movie.popularity = movieData.value(forKey: "popularity") as? String
            movie.releaseDate = movieData.value(forKey: "releaseDate") as? String
            movie.voteCount = movieData.value(forKey: "voteCount") as? Int
            movie.video = movieData.value(forKey: "video") as? Bool
            movie.voteAverage = movieData.value(forKey: "voteAverage") as? Int
            movie.category = movieData.value(forKey: "category") as? String
            
             movieList.append(movie)
        }
        
        return movieList
    }
    
    //pasre movie data manage object to movie
    public static func parseMovieDataToMovie(movieData:NSManagedObject) -> Movie{
        
        let movie = Movie()
        movie.id = movieData.value(forKey: "id") as? Int
        movie.posterPath = movieData.value(forKey: "posterPath") as? String
        movie.adult = movieData.value(forKey: "adult") as? Bool
        movie.overview = movieData.value(forKey: "overview") as? String
        movie.originalTitle = movieData.value(forKey: "originalTitle") as? String
        movie.originalLanguage = movieData.value(forKey: "originalLanguage") as? String
        movie.title = movieData.value(forKey: "title") as? String
        movie.backdropPath = movieData.value(forKey: "backdropPath") as? String
        movie.popularity = movieData.value(forKey: "popularity") as? String
        movie.releaseDate = movieData.value(forKey: "releaseDate") as? String
        movie.voteCount = movieData.value(forKey: "voteCount") as? Int
        movie.video = movieData.value(forKey: "video") as? Bool
        movie.voteAverage = movieData.value(forKey: "voteAverage") as? Int
        movie.category = movieData.value(forKey: "category") as? String
        
        return movie
    }
    
}

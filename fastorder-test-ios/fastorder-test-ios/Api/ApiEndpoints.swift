//
//  ApiEndpoints.swift
//  colnect-message
//
//  Created by AGM Tazim on 9/3/20.
//  Copyright © 2020 MacBook Pro. All rights reserved.
//

import UIKit

class ApiEndpoints: NSObject {

    public static let BASE_URL_API:String = ApiManager.API_SERVER_BASE_URL+"/"+ApiManager.API_VERSION
    public static let BASE_URL_IMAGE:String = ApiManager.API_SERVER_BASE_URL+"/"+ApiManager.API_VERSION
    
    //Get movie
    public static let URI_GET_POPULAR_MOVIE_LIST: String = BASE_URL_API+"/movie/popular"
    public static let URI_GET_TOP_RATED_MOVIE_LIST: String = BASE_URL_API+"/movie/top_rated"
    public static let URI_GET_UPCOMMING_MOVIE_LIST: String = BASE_URL_API+"/movie/upcoming"
    
}

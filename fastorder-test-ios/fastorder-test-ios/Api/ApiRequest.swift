//
//  ApiRequest.swift
//  fastorder-test-ios
//
//  Created by Mostafizur Rahman on 28/4/20.
//  Copyright © 2020 FastOrder. All rights reserved.
//

import UIKit

class ApiRequest {
    
    public static func get(url:String, param:String, completeionHandler: @escaping (_ response:NSDictionary?) -> Void) -> Void{
        let session = URLSession.shared
        guard let url = URL(string: "\(url)?\(param)") else{
            print("Invalid Url !!")
            return
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")

        let task = session.dataTask(with: request) { data, response, error in

            if error != nil || data == nil {
                print("Client error!")
                return
            }

            guard let response = response as? HTTPURLResponse, (200...299).contains(response.statusCode) else {
                print("Server error!")
                return
            }

            guard let mime = response.mimeType, mime == "application/json" else {
                print("Wrong MIME type!")
                return
            }

            guard let responseData = data else{
                print("No data")
                
                return
            }
            
            let result = try? JSONSerialization.jsonObject(with: responseData, options: [])
            
            completeionHandler(result as? NSDictionary)
        }

        task.resume()
    }
}

//
//  ApiManager.swift
//  colnect-message
//
//  Created by AGM Tazim on 9/3/20.
//  Copyright © 2020 MacBook Pro. All rights reserved.
//

import UIKit

class ApiManager: NSObject {
    public static let API_KEY:String = "b9ee58a057d7f2000cc17eda1bce31b1"
    public static let API_SERVER_BASE_URL:String = "https://api.themoviedb.org"
    public static let API_VERSION = "3"
    
    public static let IMAGE_SERVER_BASE_URL:String = "https://image.tmdb.org/t/p/w500"

}

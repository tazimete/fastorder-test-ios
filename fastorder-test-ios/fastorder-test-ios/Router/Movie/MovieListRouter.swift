//
//  MovieListRouter.swift
//  fastorder-test-ios
//
//  Created by Mostafizur Rahman on 30/4/20.
//  Copyright © 2020 FastOrder. All rights reserved.
//

import UIKit

/// This class  is used as a presenter-router  communcation model
class MovieListRouter : NSObject {
    public var movie: Movie?
    
    //create movie list module
    class func creatMovieListModule(movieListViewController : MovieListViewController) -> Void {
        let presenter: MovieListPresenterProtocol = MovieListPresenter()
        
        movieListViewController.presenter = presenter
        movieListViewController.presenter?.router = MovieListRouter()
        movieListViewController.presenter?.view = movieListViewController
        movieListViewController.presenter?.interactor = MovieListInteractor()
        movieListViewController.presenter?.interactor?.presenter = presenter
    }
}


//MARK: MovieListRouterProtocol
extension MovieListRouter : MovieListRouterProtocol{
    /// This method is used to show selected movie details
    /// - Parameters:
    ///   - movie: selected movie
    ///   - view: View Controller from Movie details view controller  to be shown 
    @objc func showMovieDetailsViewController(with movie: Movie, from view: UIViewController) {
        self.movie = Optional(movie)
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MovieDetailsViewController") as! MovieDetailsViewController
        vc.movie = Optional(movie)
        view.navigationController?.pushViewController(vc, animated: true)
    }
}

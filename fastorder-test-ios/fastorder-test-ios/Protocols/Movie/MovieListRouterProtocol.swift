//
//  MovieListRouterProtocol.swift
//  fastorder-test-ios
//
//  Created by Mostafizur Rahman on 30/4/20.
//  Copyright © 2020 FastOrder. All rights reserved.
//

import UIKit

//MARK: MovieListRouterProtocol
/// This protocols  is used as a presenter-router  communcation model
public protocol MovieListRouterProtocol : class{
    var movie: Movie? {get set}
    
    
    /// This method is used to show selected movie details
    /// - Parameters:
    ///   - movie: selected movie
    ///   - view: View Controller from Movie details view controller  to be shown 
    func showMovieDetailsViewController(with movie: Movie, from view: UIViewController)
}

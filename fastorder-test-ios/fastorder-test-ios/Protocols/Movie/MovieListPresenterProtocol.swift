//
//  MovieListPresenterProtocol.swift
//  fastorder-test-ios
//
//  Created by Mostafizur Rahman on 30/4/20.
//  Copyright © 2020 FastOrder. All rights reserved.
//

import UIKit

//MARK: MovieListPresenterProtocol
/// This protocols  is used as a view-presenter, interactor-presenter  communcation model
protocol MovieListPresenterProtocol : class{
    var popularMovieList: [Movie]? {get set}
    var popularMovieListAll: [Movie]? {get set}
    var topRatedMovieList: [Movie]? {get set}
    var topRatedMovieListAll: [Movie]? {get set}
    var upcommingMovieList: [Movie]? {get set}
    var upcommingMovieListAll: [Movie]? {get set}
    
    var interactor: MovieListInteractorProtocol? {get set}
    var view: MovieListViewProtocol? {get set}
    var router: MovieListRouterProtocol? {get set}
    
    //when view did load initialized
    func viewDidLoad()
    
    //when loading all movie data
    func loadAllMovieData()
    
    
    /// This method is used to communicate with interactor to load popular movie data 
    /// - Parameter page: page number for loading movie data
    func loadPopularMovieData(page:Int)
    
    /// This method is used to communicate with interactor to load top rated movie data
    /// - Parameter page: page number for loading movie data
    func loadTopRatedMovieData(page:Int)
    
    /// This method is used to communicate with interactor to load upcomming movie data
    /// - Parameter page: page number for loading movie data
    func loadUpcommingMovieData(page:Int)
    
    
    /// This method is used to recieve popular movie list from interactor  after loading from server or local db
    /// - Parameter movieList: list of popular movies comes from server or local db
    func didPopularMovieDataLoaded(movieList:[Movie])
    
    /// This method is used to recieve top rated movie list from interactor  after loading from server or local db
    /// - Parameter movieList: list of  top rated movies comes from server or local db
    func didTopRatedMovieDataLoaded(movieList: [Movie])
    
    /// This method is used to recieve upcomming movie list from interactor  after loading from server or local db
    /// - Parameter movieList: list of upcomming movies comes from server or local db
    func didUpcommingMovieDataLoaded(movieList: [Movie])
    
    
    /// This method is used to get popular movie from the list depending on position
    /// - Parameter position: position of the popular movie in the list
    func getPopularMovie(at position: Int) -> Movie?
      
    /// This method is used to get top rated movie from the list depending on position
    /// - Parameter position: position of the top rated movie in the list
    func getTopRatedMovie(at position: Int) -> Movie?
      
    /// This method is used to get upcomming movie from the list depending on position
    /// - Parameter position: position of the upcomming movie in the list
    func getUpcommingMovie(at position: Int) -> Movie?
    
    
    /// This method is used to show selected movie details
    /// - Parameters:
    ///   - movie: selected movie
    ///   - view: Movie details view controller
    func showMovieDetailsViewController(with movie: Movie, from view: UIViewController)
    
    
    /// This method is used to search movie vy movie title
    /// - Parameter searchText: text of movie title
    func searchMovieByTitle(searchText:String)
    
    
    /// This method is used to reset movie data after seacrhing 
    func resetMovieData()
}

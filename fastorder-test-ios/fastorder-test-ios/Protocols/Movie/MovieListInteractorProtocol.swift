//
//  MovieListInteractorProtocol.swift
//  fastorder-test-ios
//
//  Created by Mostafizur Rahman on 30/4/20.
//  Copyright © 2020 FastOrder. All rights reserved.
//

import UIKit

//MARK: MovieListInteractorProtocol
/// This protocols  is used as a presenter-interactor  communcation model
protocol MovieListInteractorProtocol : class{
    var popularMovieList: [Movie]? {get set}
    var popularMovieListAll: [Movie]? {get set}
    var topRatedMovieList: [Movie]? {get set}
    var topRatedMovieListAll: [Movie]? {get set}
    var upcommingMovieList: [Movie]? {get set}
    var upcommingMovieListAll: [Movie]? {get set}
    
    var presenter: MovieListPresenterProtocol? {get set}
    
    /// This method is used to receive call back from presenter for loadng all movie list
    func loadAllMovieData()
    
    /// This method is used to call  from presenter to load popular movie data
    /// - Parameter page: page number for loading movie data
    func loadPopularMovieData(page:Int)
    
    /// This method is used to call  from presenter to load top rated movie data
    /// - Parameter page: page number for loading movie data
    func loadTopRatedMovieData(page:Int)
    
    /// This method is used to call  from presenter to load upcomming movie data
    /// - Parameter page: page number for loading movie data
    func loadUpcommingMovieData(page:Int)
}

//
//  MovieListItemDelegate.swift
//  fastorder-test-ios
//
//  Created by Mostafizur Rahman on 30/4/20.
//  Copyright © 2020 FastOrder. All rights reserved.
//

import UIKit

//MARK: MovieListItemDelegate

/// This protocols is used as a callback or listener for selecting movie item 
public protocol MovieListItemDelegate:NSObjectProtocol{
    
    /// Delegate to detect tap on movie list item
    /// - Parameters:
    ///   - movieIndex: movieIndex position of movie in the list
    ///   - movie: Selected movie object
    ///   - category: category of the movie
    func didTapMovieListItem(movieIndex:Int, movie:Movie, category:MovieCategory)
    
    
    
    /// Delegate method to detect pagination (Lazy loading)
    /// - Parameters:
    ///   - category: category of the movie
    ///   - page: page number to fecth movie data 
    func onPaginateData(category: MovieCategory, page:Int)
}

//
//  MovieListPresenterDelegate.swift
//  fastorder-test-ios
//
//  Created by Mostafizur Rahman on 30/4/20.
//  Copyright © 2020 FastOrder. All rights reserved.
//

import UIKit

//MARK: MovieListViewProtocol
/// This protocols  is used as a presenter-view communcation model
public protocol MovieListViewProtocol : class{

    /// This method is used to receive call back from presenter when view did load
    func initialize()
    
    /// This method is used to receive  popular movie data from  presenter  after  loading from server or local database
    /// - Parameter movieList: List of movie
    func didPopularMovieDataLoaded(movieList:[Movie])
    
    /// This method is used to receive  top rated movie data from  presenter  after  loading from server or local database
    /// - Parameter movieList: List of movie
    func didTopRatedMovieDataLoaded(movieList: [Movie])
    
    /// This method is used to receive  upcomming movie data from  presenter  after  loading from server or local database
    /// - Parameter movieList: List of movie
    func didUpcommingMovieDataLoaded(movieList: [Movie])
    
    
    /// This method is used to receive  callback  from  presenter  after  loading search data locally
    func didSearchComplete()
    
    
    /// This method is used to receive  callback  from  presenter  after  reset movie data
    func didResetMovieData()
}

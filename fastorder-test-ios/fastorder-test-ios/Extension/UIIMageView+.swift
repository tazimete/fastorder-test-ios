//
//  UIIMageViw+.swift
//  fastorder-test-ios
//
//  Created by Mostafizur Rahman on 29/4/20.
//  Copyright © 2020 FastOrder. All rights reserved.
//

import UIKit


public extension UIImageView {
    public func loadImageFromURL(fromURL url: String) {
        guard let imageURL = URL(string: url) else {
            return
        }
        
        let cache =  URLCache.shared
        let request = URLRequest(url: imageURL)
        DispatchQueue.global(qos: .userInitiated).async {
            if let data = cache.cachedResponse(for: request)?.data, let image = UIImage(data: data) {
                DispatchQueue.main.async {
                    self.transition(toImage: image)
                }
            } else {
                URLSession.shared.dataTask(with: request, completionHandler: { (data, response, error) in
                    if let data = data, let response = response, ((response as? HTTPURLResponse)?.statusCode ?? 500) < 300, let image = UIImage(data: data) {
                        let cachedData = CachedURLResponse(response: response, data: data)
                        cache.storeCachedResponse(cachedData, for: request)
                        DispatchQueue.main.async {
                            self.transition(toImage: image)
                        }
                    }
                }).resume()
            }
        }
    }
    
    public func transition(toImage image: UIImage?) {
        UIView.transition(with: self, duration: 0.3,
                          options: [.transitionCrossDissolve],
                          animations: {
                            let resizedImage = Helper.resizeImageToFitView(image: image!, maxWidth: self.frame.width, maxHeight: self.frame.height)
                            self.image = resizedImage
        },
                          completion: nil)
    }
}

//
//  Helper.swift
//  colnect-message
//
//  Created by AGM Tazim on 9/3/20.
//  Copyright © 2020 MacBook Pro. All rights reserved.
//

import UIKit

/// This class provides helper methods to reuse and simplify codes
class Helper: NSObject {
    
    ///Get string  height depending on string width and font size
    /// - Parameters:
    ///   - text: String to get its height
    ///   - width:  Maximum width of string text
    ///   - font: applied font of string text to detect its size
    public static func getTextHeight(text: String, width:CGFloat, font:UIFont) -> CGFloat {
        
        if (text == "") || text == nil || text == nil {
            return 0
        }
        
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: 0))
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        label.font = font
        label.text = text
        label.sizeToFit()
        
        return (label.frame.size.height)
    }
    
    ///Get string  width depending on string height and font size
    /// - Parameters:
    ///   - text: String to get its width
    ///   - height:  Maximum height of string text
    ///   - font: applied font of string text to detect its size
    public static func getTextWidth(text: String, height:CGFloat, font:UIFont) -> CGFloat {
        
        if (text == "") || text == nil || text == nil {
            return 0
        }
        
        let label = UILabel(frame: CGRect(x: 0, y: 0, width:0, height: height))
        label.lineBreakMode = .byWordWrapping
        label.numberOfLines = 0
        label.font = font
        label.text = text
        label.sizeToFit()
        
        return (label.frame.size.width)
    }
    
    
    ///Get string  size depending on string width and font size
    /// - Parameters:
    ///   - text: String to get its size
    ///   - width: Maximum width of string text
    ///   - font: applied font of string text to detect its size
    public static func textSizeThatFitsWidth(_ width: CGFloat, text: String, font: UIFont) -> CGSize {
        let textContainer: NSTextContainer = {
            let size = CGSize(width: width, height: .greatestFiniteMagnitude)
            let container = NSTextContainer(size: size)
            container.lineFragmentPadding = 0
            return container
        }()
        
        let textStorage = self.replicateUITextViewNSTextStorage(text, font: font)
        let layoutManager: NSLayoutManager = {
            let layoutManager = NSLayoutManager()
            layoutManager.addTextContainer(textContainer)
            textStorage.addLayoutManager(layoutManager)
            return layoutManager
        }()
        
        let rect = layoutManager.usedRect(for: textContainer)
        return rect.size
    }
    
    ///Get string  text stoarge depending on its  font size
    /// - Parameters:
    ///   - text: String to get its text storage
    ///   - font: applied font of string text to detect its size
    public static  func replicateUITextViewNSTextStorage(_ text: String, font: UIFont) -> NSTextStorage {
        // See https://github.com/badoo/Chatto/issues/129
        return NSTextStorage(string: text, attributes: [ NSAttributedString.Key.font: font, NSAttributedString.Key(rawValue: "NSOriginalFont"): font ])
    }
    
    
    ///Applies  masked view with custom corner raius of top, bottom, left, right
    /// - Parameters:
    ///   - view: View to applie  masked view with custom corner raius of top, bottom, left, right
    ///   - topLeftRadius:  Maximum topLeft radius of mask view
    ///   - bottomLeftRadius: Maximum bottomLeft radius of mask view
    ///   - bottomRightRadius: Maximum bottomRight radius of mask view
    ///   - topRightRadius: Maximum topRight radius of mask view
    ///   - width: Maximum width  of mask view
    ///   - height: Maximum height  of mask view
    public static func maskView(_ view: UIView?, topLeftRadius: CGFloat, bottomLeftRadius: CGFloat, bottomRightRadius: CGFloat, topRightRadius: CGFloat, width: CGFloat, height: CGFloat) {
        
        let minx: CGFloat = 0
        let miny: CGFloat = 0
        let maxx = width
        let maxy = height
        
        let path = UIBezierPath()
        path.move(to: CGPoint(x: minx + topLeftRadius, y: miny))
        path.addLine(to: CGPoint(x: maxx - topRightRadius, y: miny))
        path.addArc(withCenter: CGPoint(x: maxx - topRightRadius, y: miny + topRightRadius), radius: topRightRadius, startAngle: CGFloat(3 * (Double.pi / 2)), endAngle: 0, clockwise: true)
        path.addLine(to: CGPoint(x: maxx, y: maxy - bottomRightRadius))
        path.addArc(withCenter: CGPoint(x: maxx - bottomRightRadius, y: maxy - bottomRightRadius), radius: bottomRightRadius, startAngle: 0, endAngle: CGFloat((Double.pi / 2)), clockwise: true)
        path.addLine(to: CGPoint(x: minx + bottomLeftRadius, y: maxy))
        path.addArc(withCenter: CGPoint(x: minx + bottomLeftRadius, y: maxy - bottomLeftRadius), radius: bottomLeftRadius, startAngle: CGFloat((Double.pi / 2)), endAngle: .pi, clockwise: true)
        path.addLine(to: CGPoint(x: minx, y: miny + topLeftRadius))
        path.addArc(withCenter: CGPoint(x: minx + topLeftRadius, y: miny + topLeftRadius), radius: topLeftRadius, startAngle: .pi, endAngle: CGFloat(3 * (Double.pi / 2)), clockwise: true)
        path.close()
        
        let maskLayer = CAShapeLayer()
        maskLayer.path = path.cgPath
        view?.layer.mask = maskLayer
    }
    
    
    ///Applies  masked view with custom corner raius of top, bottom, left, right and elebation (borderShadow)
    /// - Parameters:
    ///   - view: View to applie  masked view with custom corner raius of top, bottom, left, right
    ///   - topLeftRadius:  Maximum topLeft radius of mask view
    ///   - bottomLeftRadius: Maximum bottomLeft radius of mask view
    ///   - bottomRightRadius: Maximum bottomRight radius of mask view
    ///   - topRightRadius: Maximum topRight radius of mask view
    ///   - width: Maximum width  of mask view
    ///   - height: Maximum height  of mask view
    ///   - elevation: Maximum border shadow  of mask view
    public static func maskViewWithElevation(_ view: UIView?, topLeftRadius: CGFloat, bottomLeftRadius: CGFloat, bottomRightRadius: CGFloat, topRightRadius: CGFloat, width: CGFloat, height: CGFloat, elevation: CGFloat) {
        
        let minx: CGFloat = 0
        let miny: CGFloat = 0
        let maxx = width
        let maxy = height
        
        let path = UIBezierPath()
        path.move(to: CGPoint(x: minx + topLeftRadius, y: miny))
        path.addLine(to: CGPoint(x: maxx - topRightRadius, y: miny))
        path.addArc(withCenter: CGPoint(x: maxx - topRightRadius, y: miny + topRightRadius), radius: topRightRadius, startAngle: CGFloat(3 * (Double.pi / 2)), endAngle: 0, clockwise: true)
        path.addLine(to: CGPoint(x: maxx, y: maxy - bottomRightRadius))
        path.addArc(withCenter: CGPoint(x: maxx - bottomRightRadius, y: maxy - bottomRightRadius), radius: bottomRightRadius, startAngle: 0, endAngle: CGFloat((Double.pi / 2)), clockwise: true)
        path.addLine(to: CGPoint(x: minx + bottomLeftRadius, y: maxy))
        path.addArc(withCenter: CGPoint(x: minx + bottomLeftRadius, y: maxy - bottomLeftRadius), radius: bottomLeftRadius, startAngle: CGFloat((Double.pi / 2)), endAngle: .pi, clockwise: true)
        path.addLine(to: CGPoint(x: minx, y: miny + topLeftRadius))
        path.addArc(withCenter: CGPoint(x: minx + topLeftRadius, y: miny + topLeftRadius), radius: topLeftRadius, startAngle: .pi, endAngle: CGFloat(3 * (Double.pi / 2)), clockwise: true)
        path.close()
        
        let maskLayer = CAShapeLayer()
        maskLayer.path = path.cgPath
        maskLayer.shadowPath = path.cgPath
        maskLayer.shadowColor = UIColor.black.cgColor
        maskLayer.shadowOffset = CGSize(width: 0, height: elevation)
        maskLayer.shadowRadius = elevation
        maskLayer.shadowOpacity = 0.24
        view?.layer.mask = maskLayer
    }
    
    
    ///Applies  masked view with custom corner raius of top, bottom, left, right and border color
    /// - Parameters:
    ///   - view: View to applie  masked view with custom corner raius of top, bottom, left, right
    ///   - topLeftRadius:  Maximum topLeft radius of mask view
    ///   - bottomLeftRadius: Maximum bottomLeft radius of mask view
    ///   - bottomRightRadius: Maximum bottomRight radius of mask view
    ///   - topRightRadius: Maximum topRight radius of mask view
    ///   - width: Maximum width  of mask view
    ///   - height: Maximum height  of mask view
    ///   - color: Border color  of mask view
    public static func maskView(withBorder view: UIView?, topLeftRadius: CGFloat, bottomLeftRadius: CGFloat, bottomRightRadius: CGFloat, topRightRadius: CGFloat, width: CGFloat, height: CGFloat, color: UIColor?) {
        
        let minx: CGFloat = 0
        let miny: CGFloat = 0
        let maxx = width
        let maxy = height
        
        let path = UIBezierPath()
        path.move(to: CGPoint(x: minx + topLeftRadius, y: miny))
        path.addLine(to: CGPoint(x: maxx - topRightRadius, y: miny))
        path.addArc(withCenter: CGPoint(x: maxx - topRightRadius, y: miny + topRightRadius), radius: topRightRadius, startAngle: CGFloat(3 * (Double.pi / 2)), endAngle: 0, clockwise: true)
        path.addLine(to: CGPoint(x: maxx, y: maxy - bottomRightRadius))
        path.addArc(withCenter: CGPoint(x: maxx - bottomRightRadius, y: maxy - bottomRightRadius), radius: bottomRightRadius, startAngle: 0, endAngle: CGFloat((Double.pi / 2)), clockwise: true)
        path.addLine(to: CGPoint(x: minx + bottomLeftRadius, y: maxy))
        path.addArc(withCenter: CGPoint(x: minx + bottomLeftRadius, y: maxy - bottomLeftRadius), radius: bottomLeftRadius, startAngle: CGFloat((Double.pi / 2)), endAngle: .pi, clockwise: true)
        path.addLine(to: CGPoint(x: minx, y: miny + topLeftRadius))
        path.addArc(withCenter: CGPoint(x: minx + topLeftRadius, y: miny + topLeftRadius), radius: topLeftRadius, startAngle: .pi, endAngle: CGFloat(3 * (Double.pi / 2)), clockwise: true)
        path.close()
        
        let maskLayer = CAShapeLayer()
        maskLayer.path = path.cgPath
        view?.layer.mask = maskLayer
        
        let borderLayer = CAShapeLayer()
        borderLayer.path = maskLayer.path // Reuse the Bezier path
        borderLayer.fillColor = UIColor.clear.cgColor
        borderLayer.strokeColor = color?.cgColor
        borderLayer.lineWidth = 2
        borderLayer.frame = view?.bounds ?? CGRect.zero
        view?.layer.addSublayer(borderLayer)
    }
    
    /// Resize image that fits uiimage view
    ///
    /// - Parameters:
    ///   - image: image to resize
    ///   - maxWidth: Maximum width if image
    ///   - maxHeight: Maximum height of image
    /// - Returns: Resized UIImage object
    public static func resizeImageToFitView(image:UIImage, maxWidth : CGFloat, maxHeight : CGFloat) -> UIImage? {
        let imgRatio : CGFloat = image.size.height/image.size.width
        
        var actualHeight : CGFloat = image.size.height
        let actualWidth : CGFloat = maxWidth//image.size.width // supposed to be the screen width
        
        
        if(imgRatio >= 1.0){ // portrait
            actualHeight = actualWidth * imgRatio
            if(maxHeight < actualHeight){
                actualHeight = maxHeight; // image will get cut off
                
            }
            
        }else { // landscape
            actualHeight = actualWidth * imgRatio
        }
        
        
        var rect = CGRect(x: 0.0, y: 0.0, width: actualWidth, height: actualHeight);
        UIGraphicsBeginImageContext(rect.size);
        image.draw(in: rect)
        var resizedImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        return resizedImage;
    }

    
}

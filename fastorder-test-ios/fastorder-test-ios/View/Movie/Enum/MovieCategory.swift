//
//  MovieCategory.swift
//  fastorder-test-ios
//
//  Created by Mostafizur Rahman on 29/4/20.
//  Copyright © 2020 FastOrder. All rights reserved.
//

import UIKit

//emun for movie category
public enum MovieCategory:Int {
    case POPULAR = 0
    case TOP_RATED = 1
    case UPCOMMING = 2
}

//enum for movie category title
public enum MovieCategoryTitle:String {
    case POPULAR = "Popular"
    case TOP_RATED = "Top Rated"
    case UPCOMMING = "Upcomming"
}



//
//  MovieListItemCell.swift
//  fastorder-test-ios
//
//  Created by Mostafizur Rahman on 28/4/20.
//  Copyright © 2020 FastOrder. All rights reserved.
//

import UIKit

class MovieListItemCell: UICollectionViewCell {
    
    //MARK: Outlets
    @IBOutlet weak var uivContainer: UIView!
    @IBOutlet weak var ivMoviePoster: UIImageView!
    @IBOutlet weak var lblMovieName: UILabel!
    @IBOutlet weak var lblRating: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}

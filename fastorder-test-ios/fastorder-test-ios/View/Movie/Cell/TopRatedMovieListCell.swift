//
//  TopRatedMovieListCell.swift
//  fastorder-test-ios
//
//  Created by Mostafizur Rahman on 30/4/20.
//  Copyright © 2020 FastOrder. All rights reserved.
//

import UIKit

class TopRatedMovieListCell: ScrollableCollectionViewCell {
    
    //MARK: Properties
    public let CLASS_NAME = TopRatedMovieListCell.self.description()
    public var delegate:MovieListItemDelegate!
    public var movieCategory:MovieCategory!
    public var currentPage:Int!
    public var movieList: [Movie]?{
        didSet{
            print("\(self.CLASS_NAME) -- didSet -- movieList.count = \(movieList?.count)")
            self.cvMovieList.reloadData()
        }
    }
    
    //MARK: Outlets
    @IBOutlet weak var uivContainer: UIView!
    @IBOutlet weak var lblCategoryName: UILabel!
    @IBOutlet weak var cvMovieList: UICollectionView!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        initialization()
    }
    
    //MARK: Intsance Method
   //initialization
   public func initialization() -> Void {
       
       //init collectionviews
       cvMovieList.delegate = self
       cvMovieList.dataSource = self
       
       //set collection view cell item cell
       let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
       layout.sectionInset = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
       layout.minimumInteritemSpacing = 5
       layout.minimumLineSpacing = 5
       layout.scrollDirection = .horizontal
       cvMovieList.collectionViewLayout = layout
       
       //cell for food list collection view
       cvMovieList?.register(UINib(nibName: "MovieListItemCell", bundle: nil), forCellWithReuseIdentifier: "MovieListItemCell")
   }
    
    override func getLastVisibleItem() -> IndexPath {
        return cvMovieList.indexPathsForVisibleItems.last ?? IndexPath(row: 0, section: 0)
    }
    
    override func getDataCount() -> Int {
        return movieList?.count ?? 0
    }
    
    override func hasMoreData() -> Bool {
//        return totalCount > movieList.count
        return true
    }
    
    override func loadMoreData() {
        if delegate != nil{
            delegate.onPaginateData(category: movieCategory, page: currentPage)
        }
    }
}


//MARK: Collection View
extension TopRatedMovieListCell : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return movieList?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: cvMovieList.frame.width/3, height: 250)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MovieListItemCell", for: indexPath) as! MovieListItemCell
        let movie = movieList?[indexPath.row]
        
        cell.lblMovieName.text = movie?.title
        cell.lblRating.text = "\(movie?.voteCount ?? 0)"
        cell.ivMoviePoster.loadImageFromURL(fromURL: movie?.posterPath ?? "")
        
        print("\(self.CLASS_NAME) -- cellForItemAt -- movie.category = \(movie?.category ?? "")")
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if delegate != nil {
            let movie = movieList?[indexPath.row]
            delegate.didTapMovieListItem(movieIndex: indexPath.row, movie: movie ?? Movie(), category: movieCategory)
        }
    }
}

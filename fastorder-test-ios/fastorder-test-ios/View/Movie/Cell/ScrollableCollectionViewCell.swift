//
//  KothaCollectionViewCell.swift
//  Kotha App
//
//  Created by Mostafizur Rahman on 4/3/20.
//  Copyright © 2020 interspeed.com.bd. All rights reserved.
//

import UIKit

class ScrollableCollectionViewCell: UICollectionViewCell, UIScrollViewDelegate {
    public var lastContentOffset:CGFloat = 0.0
    
    enum ScrollDirection : Int {
        case none
        case right
        case left
        case up
        case down
        case crazy
    }
    
    public func getScrollDirection(scrollView:UIScrollView) -> ScrollDirection{
        var scrollDirection: ScrollDirection = .none

           if lastContentOffset > scrollView.contentOffset.x {
               scrollDirection = .left
           } else if lastContentOffset < scrollView.contentOffset.x {
               scrollDirection = .right
           }

           lastContentOffset = scrollView.contentOffset.x

           return scrollDirection
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        onEndScrolling(scrollView: scrollView)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        onEndScrolling(scView: scrollView)
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if (!decelerate) {
            onEndScrolling(scView: scrollView)
        }
    }
    
    // To be overriden by child classes
    public func hasMoreData() -> Bool{
        fatalError("Must Override")
        return false
    }
    
    public func loadMoreData() -> Void{
        // LUC: 20191210
        fatalError("Must Override")
    }
    
    public func getFirstVisibleItem() -> IndexPath{
        return IndexPath(row: 0, section: 0)
    }
    
    public func getLastVisibleItem() -> IndexPath{
        fatalError("Must Override")
        return IndexPath(row: 0, section: 0)
    }
    
    public func getDataCount() -> Int{
        fatalError("Must Override")
        return 0
    }
    
    public func onEndScrolling(scView: UIScrollView) -> Void{
        if getScrollDirection(scrollView: scView) == .right{
            if(hasMoreData()){
                
                let firstVisibleItem = getFirstVisibleItem()
                let lastVisibleItem = getLastVisibleItem()
                          
                print("ScrollableCollectionViewCell -- scrollViewDidScroll() -- down, lastVisibleItem = \(lastVisibleItem)")
                
                  if ( lastVisibleItem.row > getDataCount()-2) {
                      loadMoreData();
                  }
                
            }
        }
    }
    
}

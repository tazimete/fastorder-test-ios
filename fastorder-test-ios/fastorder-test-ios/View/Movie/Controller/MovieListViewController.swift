//
//  MovieListViewController.swift
//  fastorder-test-ios
//
//  Created by Mostafizur Rahman on 28/4/20.
//  Copyright © 2020 FastOrder. All rights reserved.
//

import UIKit

class MovieListViewController: UIViewController {
    //MARK: Properties
    public let CLASS_NAME = MovieListViewController.self.description()
    public var currentPagePopular = 1, currentPageTopRated = 1, currentPageUpcomming = 1
    public var presenter:MovieListPresenterProtocol?
    
    //MARK: Outlets
    @IBOutlet weak var cvMovieList: UICollectionView!
    lazy var searchController:UISearchController = UISearchController(searchResultsController: nil)
    

    override func viewDidLoad() {
        super.viewDidLoad()

        //initialize
        MovieListRouter.creatMovieListModule(movieListViewController: self)
        presenter?.viewDidLoad()
        
        // load movie data
        presenter?.loadAllMovieData()
    }
    
    //MARK: Instance Method
    //initialize search controller
    private func initializesearchController() -> Void{
        //init search bar
        searchController.searchBar.placeholder = "Search Movie"
        searchController.searchBar.delegate = self
        
        if #available(iOS 11.0, *) {
            self.navigationItem.hidesSearchBarWhenScrolling = true
            self.navigationController?.navigationBar.prefersLargeTitles = true
            self.navigationItem.searchController = searchController
        } else {
            let leftNavBarButton = UIBarButtonItem(customView: searchController.view)
            self.navigationItem.leftBarButtonItem = leftNavBarButton
        }
    }
}

//MARK: MovieListIMovieListViewProtocoltemDelegate
/// This protocols  is used as a presenter-view communcation model
extension MovieListViewController : MovieListViewProtocol{
     /// This method is used to receive call back from presenter when view did load
    @objc func initialize() {
        //initialize search controller
        initializesearchController()
        
        //init collectionviews
        cvMovieList.delegate = self
        cvMovieList.dataSource = self
        
        //set collection view cell item cell
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 10
        layout.scrollDirection = .vertical
        cvMovieList.collectionViewLayout = layout
        
        //cell for food list collection view
        cvMovieList?.register(UINib(nibName: "PopularMovieListCell", bundle: nil), forCellWithReuseIdentifier: "PopularMovieListCell")
        cvMovieList?.register(UINib(nibName: "TopRatedMovieListCell", bundle: nil), forCellWithReuseIdentifier: "TopRatedMovieListCell")
        cvMovieList?.register(UINib(nibName: "UpcommingMovieListCell", bundle: nil), forCellWithReuseIdentifier: "UpcommingMovieListCell")
    }
    
    /// This method is used to receive  popular movie data from  presenter  after  loading from server or local database
    /// - Parameter movieList: List of movie
    @objc func didPopularMovieDataLoaded(movieList: [Movie]) {
        DispatchQueue.main.async {
            self.cvMovieList.reloadItems(at: [IndexPath(row: 0, section: 0)])
        }
    }
    
    /// This method is used to receive  top rated movie data from  presenter  after  loading from server or local database
    /// - Parameter movieList: List of movie
    @objc func didTopRatedMovieDataLoaded(movieList: [Movie]) {
        DispatchQueue.main.async {
            self.cvMovieList.reloadItems(at: [IndexPath(row: 1, section: 0)])
        }
    }
       
    /// This method is used to receive  upcomming movie data from  presenter  after  loading from server or local database
    /// - Parameter movieList: List of movie
    @objc func didUpcommingMovieDataLoaded(movieList: [Movie]) {
        DispatchQueue.main.async {
            self.cvMovieList.reloadItems(at: [IndexPath(row: 2, section: 0)])
        }
    }
    
    /// This method is used to receive  callback  from  presenter  after  loading search data locally
    @objc func didSearchComplete() {
        self.cvMovieList.reloadData()
    }
    
    /// This method is used to receive  callback  from  presenter  after  reset movie data
    @objc func didResetMovieData() {
        self.cvMovieList.reloadData()
    }
}


//MARK: Collection View
extension MovieListViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        var count = 3
        
        return count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var height = 270
        
        if indexPath.row == MovieCategory.POPULAR.rawValue{
            if (presenter?.popularMovieList?.count ?? 0) == 0{
                height = 0
            }
        }else if indexPath.row == MovieCategory.TOP_RATED.rawValue{
            if (presenter?.topRatedMovieList?.count ?? 0) == 0{
                height = 0
            }
        }else if indexPath.row == MovieCategory.UPCOMMING.rawValue{
            if (presenter?.upcommingMovieList?.count ?? 0) == 0{
                height = 0
            }
        }
        
        
        return CGSize(width: cvMovieList.frame.width, height: CGFloat(height))
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        //show popular movie in first cell
        if indexPath.row == MovieCategory.POPULAR.rawValue{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PopularMovieListCell", for: indexPath) as! PopularMovieListCell
            
            cell.delegate = self
            cell.lblCategoryName.text = MovieCategoryTitle.POPULAR.rawValue
            cell.movieList = presenter?.popularMovieList
            cell.movieCategory = MovieCategory.POPULAR
            cell.currentPage = currentPagePopular
            cell.initialization()
            return cell
        }
        
        //show top rated movie in first cell
        else if indexPath.row == MovieCategory.TOP_RATED.rawValue{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TopRatedMovieListCell", for: indexPath) as! TopRatedMovieListCell
            
            cell.delegate = self
            cell.lblCategoryName.text = MovieCategoryTitle.TOP_RATED.rawValue
            cell.movieList = presenter?.topRatedMovieList
            cell.movieCategory = MovieCategory.TOP_RATED
            cell.currentPage = currentPageTopRated
            cell.initialization()
            return cell
        }
        
        //show upcomming movie in first cell
        else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "UpcommingMovieListCell", for: indexPath) as! UpcommingMovieListCell
            
            cell.delegate = self
            cell.lblCategoryName.text = MovieCategoryTitle.UPCOMMING.rawValue
            cell.movieList = presenter?.upcommingMovieList
            cell.movieCategory = MovieCategory.UPCOMMING
            cell.currentPage = currentPageUpcomming
            cell.initialization()
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.searchController.view.window?.endEditing(true)
    }
}


//MARK: UISearchBarDelegate
extension MovieListViewController : UISearchBarDelegate{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print("\(CLASS_NAME) -- searchBar -- textDidChange = \(searchText)")
        
         if (searchText.count) > 0{
            // cancel it before being performed
//            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(presenter?.searchMovieByTitle), object: nil)
            // set up a delayed function call…
//            perform(#selector(presenter?.searchMovieByTitle), with: searchText, afterDelay: 1.0)
            presenter?.searchMovieByTitle(searchText: searchText)
        }else{
            presenter?.resetMovieData()
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        presenter?.resetMovieData()
        searchBar.window?.endEditing(true)
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        presenter?.resetMovieData()
        searchBar.window?.endEditing(true)
    }
}

//MARK: MovieListItemDelegate
/// This protocols is used as a callback or listener for selecting movie item
extension MovieListViewController : MovieListItemDelegate{
    /// Delegate to detect tap on movie list item
    /// - Parameters:
    ///   - movieIndex: movieIndex position of movie in the list
    ///   - movie: Selected movie object
    ///   - category: category of the movie
    func didTapMovieListItem(movieIndex: Int, movie: Movie, category: MovieCategory) {
        presenter?.showMovieDetailsViewController(with: movie, from: self)
    }
    
    /// Delegate method to detect pagination (Lazy loading)
    /// - Parameters:
    ///   - category: category of the movie
    ///   - page: page number to fecth movie data
    func onPaginateData(category: MovieCategory, page: Int) {
//        if category == .POPULAR{
//            currentPagePopular += 1
//            presenter?.loadPopularMovieData(page: currentPagePopular)
//        }else if category == .TOP_RATED{
//            currentPageTopRated += 1
//            presenter?.loadTopRatedMovieData(page: currentPageTopRated)
//        }else if category == .UPCOMMING{
//            currentPageUpcomming += 1
//            presenter?.loadUpcommingMovieData(page: currentPageUpcomming)
//        }
    }
}

//
//  MovieDetailsViewController.swift
//  fastorder-test-ios
//
//  Created by Mostafizur Rahman on 29/4/20.
//  Copyright © 2020 FastOrder. All rights reserved.
//

import UIKit

class MovieDetailsViewController: UIViewController {
    //MARK: Properties
    public let CLASS_NAME = MovieDetailsViewController.self.description()
    public var movie:Movie?
    
    //MARK: Outlets
    @IBOutlet weak var ivPosterImage: UIImageView!
    @IBOutlet weak var lblMovieName: UILabel!
    @IBOutlet weak var lblVote: UILabel!
    @IBOutlet weak var lblOverview: UILabel!
    @IBOutlet weak var lblPopularity: UILabel!
    @IBOutlet weak var lblVoteAverage: UILabel!
    @IBOutlet weak var lblVoteCount: UILabel!
    @IBOutlet weak var lblReleaseDate: UILabel!
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        if let movie = movie {
            ivPosterImage.loadImageFromURL(fromURL: movie.posterPath ?? "")
            lblMovieName.text = movie.title
            lblOverview.text = movie.overview
            lblVote.text = "Vote Count : \(movie.voteCount ?? 0)"
            lblPopularity.text = "Popularity : \(movie.popularity ?? "")"
            lblVoteAverage.text = "Vote Average : \(movie.voteAverage ?? 0)"
            lblVoteCount.text = "Total Vote Count : \(movie.voteCount ?? 0)"
            lblReleaseDate.text = "Relase Date : \(movie.releaseDate ?? "")"
        }
    }

}

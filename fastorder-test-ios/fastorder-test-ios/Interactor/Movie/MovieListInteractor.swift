//
//  MovieInteractor.swift
//  fastorder-test-ios
//
//  Created by Mostafizur Rahman on 30/4/20.
//  Copyright © 2020 FastOrder. All rights reserved.
//

import UIKit
import CoreData

/// This class  is used as a presenter-interactor  communcation model
class MovieListInteractor: NSObject {
    //MARK: Properties
    public let CLASS_NAME = MovieListViewController.self.description()
    public weak var presenter: MovieListPresenterProtocol?
    private var appDelegate = UIApplication.shared.delegate as! AppDelegate
    public var popularMovieList: [Movie]?, popularMovieListAll: [Movie]?
    public var topRatedMovieList: [Movie]?, topRatedMovieListAll: [Movie]?
    public var upcommingMovieList: [Movie]?, upcommingMovieListAll: [Movie]?
    
    
    //get popular movie list
    private func getPopularMovieList(page:Int) -> Void{
        ApiRequest.get(url: ApiEndpoints.URI_GET_POPULAR_MOVIE_LIST, param: "api_key=\(ApiManager.API_KEY)&page=\(page)", completeionHandler: {
           [weak self] response in
            
            print("\(self?.CLASS_NAME) -- getPopularMovieList() -- Resposne = \(response)")
            
            let popularMovieList = Parser.parseMovieApiResponse(response: response, category: MovieCategoryTitle.POPULAR.rawValue)
            
            //alert presenter
            self?.presenter?.didPopularMovieDataLoaded(movieList: popularMovieList)
            
            //insert movie list data to local database
            self?.insertMovieListToDatabase(movieList: popularMovieList, category: .POPULAR)
        })
    }
    
    //get top rated movie list
    private func getTopRatedMovieList(page:Int) -> Void{
        ApiRequest.get(url: ApiEndpoints.URI_GET_TOP_RATED_MOVIE_LIST, param: "api_key=\(ApiManager.API_KEY)&page=\(page)", completeionHandler: {
           [weak self] response in
            
            print("\(self?.CLASS_NAME) -- getTopRatedMovieList() -- Resposne = \(response)")
            
            let topRatedMovieList = Parser.parseMovieApiResponse(response: response, category: MovieCategoryTitle.TOP_RATED.rawValue)
            
            //alert presenter
            self?.presenter?.didTopRatedMovieDataLoaded(movieList: topRatedMovieList)
            
            //insert movie list data to local database
            self?.insertMovieListToDatabase(movieList: topRatedMovieList, category: .TOP_RATED)
        })
    }
    
    //get upcomming movie list
    private func getUpcommingdMovieList(page:Int) -> Void{
        ApiRequest.get(url: ApiEndpoints.URI_GET_UPCOMMING_MOVIE_LIST, param: "api_key=\(ApiManager.API_KEY)&page=\(page)", completeionHandler: {
           [weak self] response in
            
            print("\(self?.CLASS_NAME) -- getUpcommingdMovieList() -- Resposne = \(response)")
            
            let upcommingMovieList = Parser.parseMovieApiResponse(response: response, category: MovieCategoryTitle.UPCOMMING.rawValue)
            
            //alert presenter
            self?.presenter?.didUpcommingMovieDataLoaded(movieList: upcommingMovieList)
            
            //insert movie list data to local database
            self?.insertMovieListToDatabase(movieList: upcommingMovieList, category: .UPCOMMING)
        })
    }
    
    //insert movie list to database
    private func insertMovieListToDatabase(movieList:[Movie], category:MovieCategory) -> Void {
        for movie in movieList{
            if !hasMovieDataById(id: movie.id ?? 0){
                insertMovieToDatabase(movie: movie)
            }
        }
    }
    
    //insert movie to database
    private func insertMovieToDatabase(movie:Movie) -> Void {
        let context = appDelegate.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "MovieData", in: context)
        let movieData = NSManagedObject(entity: entity!, insertInto: context)
        
        movieData.setValue(movie.id ?? "", forKey: "id")
        movieData.setValue(movie.posterPath ?? "", forKey: "posterPath")
        movieData.setValue(movie.adult ?? false, forKey: "adult")
        movieData.setValue(movie.overview ?? "", forKey: "overview")
        movieData.setValue(movie.originalTitle ?? "", forKey: "originalTitle")
        movieData.setValue(movie.originalLanguage ?? "", forKey: "originalLanguage")
        movieData.setValue(movie.title ?? "", forKey: "title")
        movieData.setValue(movie.backdropPath ?? "", forKey: "backdropPath")
        movieData.setValue(movie.popularity ?? "", forKey: "popularity")
        movieData.setValue(movie.releaseDate ?? "", forKey: "releaseDate")
        movieData.setValue(movie.voteCount ?? "", forKey: "voteCount")
        movieData.setValue(movie.video ?? false, forKey: "video")
        movieData.setValue(movie.voteAverage ?? 0, forKey: "voteAverage")
        movieData.setValue(movie.category ?? "", forKey: "category")
        movieData.setValue(Date().millisecondsSince1970, forKey: "date")
        
        try? context.save()
    }
    
    //check if already data inserted
    private func hasMovieDataById(id:Int) -> Bool {
        let context = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "MovieData")
        fetchRequest.predicate = NSPredicate(format: "id = %d", id)
        fetchRequest.includesSubentities = false

        var count = 0
        count = (try? context.count(for: fetchRequest)) ?? 0

        return count > 0
    }
    
    //retrieve movie from database
    private func retrieveMovieListFromDatabase(category:MovieCategory) -> Void {
        let context = appDelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "MovieData")
        request.sortDescriptors = [NSSortDescriptor(key: "date", ascending: true)]
        request.returnsObjectsAsFaults = false
        
        if category == .POPULAR{
            request.predicate = NSPredicate(format: "category = %@", MovieCategoryTitle.POPULAR.rawValue)
        }else if category == .TOP_RATED{
            request.predicate = NSPredicate(format: "category = %@", MovieCategoryTitle.TOP_RATED.rawValue)
        }else if category == .UPCOMMING{
            request.predicate = NSPredicate(format: "category = %@", MovieCategoryTitle.UPCOMMING.rawValue)
        }
        
        let result = try? context.fetch(request)
        
        if category == .POPULAR{
            let popularMovieList = Parser.parseMovieDataListToMovie(movieDataList: result as! [NSManagedObject])
            
            //alert presenter
            self.presenter?.didPopularMovieDataLoaded(movieList: popularMovieList)
        }else if category == .TOP_RATED{
            let topRatedMovieList = Parser.parseMovieDataListToMovie(movieDataList: result as! [NSManagedObject])
            
            //alert presenter
            self.presenter?.didTopRatedMovieDataLoaded(movieList: topRatedMovieList)
        }else if category == .UPCOMMING{
            let upcommingMovieList = Parser.parseMovieDataListToMovie(movieDataList: result as! [NSManagedObject])
           
            //alert presenter
            self.presenter?.didUpcommingMovieDataLoaded(movieList: upcommingMovieList)
        }
            
    }
}

//MARK: MovieListPresenterProtocol
extension MovieListInteractor : MovieListInteractorProtocol{
    /// This method is used to receive call back from presenter for loadng all movie list
    @objc func loadAllMovieData(){
        if ConnectivityDetector.isInternetAvailable(){
            //get popular, top rated, upcomming movie list
            getPopularMovieList(page: 1)
            getTopRatedMovieList(page: 1)
            getUpcommingdMovieList(page: 1)
        }else{
            //retrive offline data
            retrieveMovieListFromDatabase(category: .POPULAR)
            retrieveMovieListFromDatabase(category: .TOP_RATED)
            retrieveMovieListFromDatabase(category: .UPCOMMING)
        }
    }
    
    /// This method is used to call  from presenter to load popular movie data
    /// - Parameter page: page number for loading movie data
    @objc func loadPopularMovieData(page: Int) {
        getPopularMovieList(page: page)
    }
    
    /// This method is used to call  from presenter to load top rated movie data
    /// - Parameter page: page number for loading movie data
    @objc func loadTopRatedMovieData(page: Int) {
        getTopRatedMovieList(page: page)
    }
    
    /// This method is used to call  from presenter to load upcomming movie data
    /// - Parameter page: page number for loading movie data
    @objc func loadUpcommingMovieData(page: Int) {
        getUpcommingdMovieList(page: page)
    }
    
}

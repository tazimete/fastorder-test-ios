//
//  MovieListPresenter.swift
//  fastorder-test-ios
//
//  Created by Mostafizur Rahman on 30/4/20.
//  Copyright © 2020 FastOrder. All rights reserved.
//

import UIKit


/// This class  is used as a view-presenter, interactor-presenter  communcation model
class MovieListPresenter: NSObject {
    public let CLASS_NAME = MovieListPresenter.self.description()
    public var router: MovieListRouterProtocol?
    public weak var view: MovieListViewProtocol?
    public var interactor: MovieListInteractorProtocol?
    
    public var popularMovieList: [Movie]?, popularMovieListAll: [Movie]?
    public var topRatedMovieList: [Movie]?, topRatedMovieListAll: [Movie]?
    public var upcommingMovieList: [Movie]?, upcommingMovieListAll: [Movie]?
}


//MARK: MovieListPresenterProtocol
extension MovieListPresenter : MovieListPresenterProtocol{
    
    //when view did load
    @objc func viewDidLoad() {
        view?.initialize()
    }
    
    //when load movie data
    @objc func loadAllMovieData() {
        interactor?.loadAllMovieData()
    }
    
    /// This method is used to communicate with interactor to load popular movie data
    /// - Parameter page: page number for loading movie data
    @objc func loadPopularMovieData(page: Int) {
        interactor?.loadPopularMovieData(page: page)
    }
    
    /// This method is used to communicate with interactor to load top rated movie data
    /// - Parameter page: page number for loading movie data
    @objc func loadTopRatedMovieData(page: Int) {
        interactor?.loadTopRatedMovieData(page: page)
    }
    
    /// This method is used to communicate with interactor to load upcomming movie data
    /// - Parameter page: page number for loading movie data
    @objc func loadUpcommingMovieData(page: Int) {
        interactor?.loadUpcommingMovieData(page: page)
    }
    
    /// This method is used to recieve popular movie list from interactor  after loading from server or local db
    /// - Parameter movieList: list of popular movies comes from server or local db
    @objc func didPopularMovieDataLoaded(movieList: [Movie]) {
        popularMovieList = movieList
        popularMovieListAll = movieList
        
        view?.didPopularMovieDataLoaded(movieList: movieList)
    }
    
    /// This method is used to recieve top rated movie list from interactor  after loading from server or local db
    /// - Parameter movieList: list of  top rated movies comes from server or local db
    @objc func didTopRatedMovieDataLoaded(movieList: [Movie]) {
        topRatedMovieList = movieList
        topRatedMovieListAll = movieList
        
        view?.didTopRatedMovieDataLoaded(movieList: movieList)
    }
       
    /// This method is used to recieve upcomming movie list from interactor  after loading from server or local db
    /// - Parameter movieList: list of upcomming movies comes from server or local db
    @objc func didUpcommingMovieDataLoaded(movieList: [Movie]) {
        upcommingMovieList = movieList
        upcommingMovieListAll = movieList
        
        view?.didUpcommingMovieDataLoaded(movieList: movieList)
    }
    
    /// This method is used to get popular movie from the list depending on position
    /// - Parameter position: position of the popular movie in the list
    @objc func getPopularMovie(at position: Int) -> Movie?{
        return popularMovieList?[position]
    }
    
    /// This method is used to get top rated movie from the list depending on position
    /// - Parameter position: position of the top rated movie in the list
    @objc func getTopRatedMovie(at position: Int) -> Movie?{
        return topRatedMovieList?[position]
    }
    
    /// This method is used to get upcomming movie from the list depending on position
    /// - Parameter position: position of the upcomming movie in the list
    @objc func getUpcommingMovie(at position: Int) -> Movie?{
        return upcommingMovieList?[position]
    }
    
    /// This method is used to show selected movie details
    /// - Parameters:
    ///   - movie: selected movie
    ///   - view: Movie details view controller
    @objc func showMovieDetailsViewController(with movie: Movie, from view: UIViewController) {
        router?.showMovieDetailsViewController(with: movie, from: view)
    }
    
    /// This method is used to search movie vy movie title
    /// - Parameter searchText: text of movie title
    @objc func searchMovieByTitle(searchText:String) -> Void{
        let text = searchText.trimmingCharacters(in: .whitespacesAndNewlines)
        
        popularMovieList = self.popularMovieListAll?.filter({
            $0.title?.lowercased().contains(text.lowercased()) ?? false
        })

        topRatedMovieList = self.topRatedMovieListAll?.filter({
            $0.title?.lowercased().contains(text.lowercased()) ?? false
        })

        upcommingMovieList = self.upcommingMovieListAll?.filter({
            $0.title?.lowercased().contains(text.lowercased()) ?? false
        })
        
        print("\(CLASS_NAME) -- showSearchMovieItems -- popularMovieList = \(popularMovieList?.count)")
        print("\(CLASS_NAME) -- showSearchMovieItems -- topRatedMovieList = \(topRatedMovieList?.count)")
        print("\(CLASS_NAME) -- showSearchMovieItems -- upcommingMovieList = \(upcommingMovieList?.count)")
        
        view?.didSearchComplete()
    }
    
    /// This method is used to reset movie data after seacrhing 
   @objc public func resetMovieData() -> Void{
        popularMovieList = self.popularMovieListAll
        topRatedMovieList = self.topRatedMovieListAll
        upcommingMovieList = self.upcommingMovieListAll
        
        view?.didResetMovieData()
    }
}

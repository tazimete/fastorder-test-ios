# README #


### What is this repository for? ###

* This is sample project to show Popular, Top Rated, Upcomming 3 categories of movie list from https://www.themoviedb.org/?language=en-US using VIPER architecture with unit test. 
* Version 1.0

### How do I get set up? ###

* Just clone this repository. All are in master branch. And open this project with Xcode 
* Dependencies - No third party library or depency used in this project 

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin